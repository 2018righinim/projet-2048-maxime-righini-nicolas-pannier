   ##Grid_2048

import random as rd

THEMES = {"0": {"name": "Default", 0: " ", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: " ", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: " ", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}


def create_grid(grid_length = 4):
    game_grid = []
    for i in range(0,grid_length):
        game_grid.append([' ' for j in range(grid_length)])
    return game_grid


def get_value_new_tile():
    T = rd.randint(1, 10)#peut prendre 10 valeurs,
                                # si cette valeur est 10 alors on crée une tuile 4 sinon on crée une tuile 2.
                                #De cette façon on a 90% de chance de créer une tuile 2.

    if T == 10:
        value = 4
    else:
        value = 2
    return value


def add_new_tile_position(grid, x, y):
    value = get_value_new_tile()
    grid[x][y] = value
    return grid


def get_all_tiles(grid):
    grid_length = len(grid)
    tiles = []
    for i in range(grid_length):
        for j in range(grid_length):
            tiles.append(get_value_tile(grid, i, j))
    return tiles


def get_empty_tiles_positions(grid):
    grid_length = len(grid)
    empty_tiles = []
    for i in range(grid_length):
        for j in range(grid_length):
            if get_value_tile(grid, i, j) == 0:
                empty_tiles.append((i,j))
    return empty_tiles


def get_value_tile(grid, x, y):
    if grid[x][y] == ' ':
        return 0
    return grid[x][y]

def get_new_position(game_grid):
    empty_tiles_positions=get_empty_tiles_positions(game_grid)
    number_of_empty_tiles=len(empty_tiles_positions)
    random = rd.randint(0,number_of_empty_tiles-1)
    random_empty_position = empty_tiles_positions[random]
    return random_empty_position #ici on veut renvoyer la position d'une case vide, mais on veux que cette case soit choisie aléatoirement parmi les cases vides de la grille de jeu...



def add_new_tile(grid):
    new_position = get_new_position(grid)
    x, y = new_position
    add_new_tile_position(grid,x,y)
    return grid


def init_game(grid_length = 4):
    grid = create_grid(grid_length)
    add_new_tile(grid)
    add_new_tile(grid)
    return grid


def grid_to_string(game_grid):
    grid_length=len(game_grid)
    string_to_print=grid_length*" ==="
    string_to_print+="\n" #passage à la ligne
    for i in range(grid_length):
        for j in range(grid_length):
            if game_grid[i][j]==' ':
                string_to_print+="|   "
            else:
                string_to_print+="| %s " %(game_grid[i][j])
        string_to_print+="| \n"
        string_to_print+=grid_length*" ==="
        string_to_print+="\n"
    return string_to_print



def grid_to_string_with_size(game_grid):
    grid_length=len(game_grid)
    size_tile=long_value(game_grid)+2 #même pour la plus grosse chaîne de caractère, on veut garder un espace à droite et à gauche dans la case
    string_to_print=grid_length*(" " +size_tile*"=") #bordure haute
    string_to_print+="\n" #passage à la ligne
    for i in range(grid_length):
        for j in range(grid_length):
            if game_grid[i][j]==' ':
                string_to_print+=("|"+size_tile*" ")
            else:
                space_left=(size_tile-len(str(game_grid[i][j])))//2 #pour garder les éléments centrés dans les tuiles, on définit l'espace qu'il faut mettre à droite et à gauche
                space_right=size_tile-len(str(game_grid[i][j])) - space_left
                string_to_print+="|"+space_left*" "+ "%s" %(game_grid[i][j]) + space_right*" "
        string_to_print+="| \n"
        string_to_print+=grid_length*(" " +size_tile*"=")
        string_to_print+="\n"
    return string_to_print



def long_value(grid): #pas de test car on utilise long_value_with_theme
    grid_length = len(grid)
    max = 0
    for i in range(grid_length):
        for j in range(grid_length):
            if len(str(grid[i][j]))>max:
                max=len(str(grid[i][j]))
    return max


def long_value_with_theme(grid, theme):
    grid_length = len(grid)
    max = 0
    for i in range(grid_length):
        for j in range(grid_length):
            num = get_value_tile(grid, i, j)
            value = THEMES[theme][num]
            l = str(value)
            if len(l) > max:
                max = len(l)
    return max

def grid_to_sting_with_size_and_theme(grid, theme):
    grid_length=len(grid)
    size_tile=long_value_with_theme(grid, theme)+2 #même pour la plus grosse chaîne de caractère, on veut garder un espace à droite et à gauche dans la case
    string_to_print=grid_length*(" " +size_tile*"=") #bordure haute
    string_to_print+="\n" #passage à la ligne
    for i in range(grid_length):
        for j in range(grid_length):
            if grid[i][j]=="":
                string_to_print+=("|"+size_tile*" ")
            else:
                space_left=(size_tile-len(THEMES[theme][grid[i][j]]))//2 #pour garder les éléments centrés dans les tuiles, on définit l'espace qu'il faut mettre à droite et à gauche
                space_right=size_tile-len(THEMES[theme][grid[i][j]]) - space_left
                string_to_print+="|"+space_left*" "+ THEMES[theme][grid[i][j]] + space_right*" "
        string_to_print+="| \n"
        string_to_print+=grid_length*(" " +size_tile*"=")
        string_to_print+="\n"
    return string_to_print

