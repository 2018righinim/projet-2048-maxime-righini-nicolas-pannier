    ## textual_2048

import os
os.chdir('C:\\Users\\maxime\\Desktop\\coding week 2048')
import random as rd
from grid_2048 import *


def read_player_command():
 move = input("Entrez votre commande (g (gauche), d (droite), h (haut), b(bas)):")
 if move in ['g', 'd', 'h', 'b']:
    return move
 else:
     print("il faut entrer une commande valide!")
     return read_player_command()

def read_size_grid():
   taille=input("Avec quelle taille de grille voulez-vous jouer?")
   try:
       int(taille)
       if int(taille)<=0:
           print("Il faut demander un entier strictement positif!")
           return(read_size_grid())
       else:
        return taille
   except ValueError:
       print("Il faut entrer un entier strictement positif!")
       return(read_size_grid())

def read_theme_grid():
 theme = input("Entrez le theme que vous desirez (Default, Chemistry ou Alphabet):")
 if theme in ['Default', 'Chemistry', 'Alphabet']:
    return theme
 else:
    print("il faut entrer un theme valide!")
    return read_theme_grid()


def move_row_left(row):
    new_row=[]
    is_already_fusionned=[]
    for i in range(len(row)):
        if row[i]!=0 and row[i]!=' ':
            if len(new_row)>0 and row[i]==new_row[-1] and is_already_fusionned[-1]==0:
                new_row[-1]*=2
                is_already_fusionned[-1]=1
            else:
                new_row.append(row[i])
                is_already_fusionned.append(0)
    new_row+=(len(row)-len(new_row))*[0]
    return new_row



def move_grid_left(grid):
    grid_length = len(grid)
    next_grid=[]
    for i in range(grid_length):
        new_line=[]
        for j in range(grid_length):
            new_line.append(grid[i][j])
        next_grid.append(new_line)
    for i in range(grid_length):
        next_grid[i]=move_row_left(grid[i])

    return next_grid

def move_row_right(row):
    new_row=[]
    is_already_fusionned=[]
    for i in range(len(row)-1,-1,-1):
        if row[i]!=0:
            if len(new_row)>0 and row[i]==new_row[0] and is_already_fusionned[0]==0:
                new_row[0]*=2
                is_already_fusionned[0]=1
            else:
                new_row=[row[i]]+new_row
                is_already_fusionned=[0]+is_already_fusionned
    new_row=(len(row)-len(new_row))*[0]+new_row
    return new_row



def move_grid_right(grid):
    grid_length = len(grid)
    next_grid=[]
    for i in range(grid_length):
        new_line=[]
        for j in range(grid_length):
            new_line.append(grid[i][j])
        next_grid.append(new_line)
    for i in range(grid_length):
        next_grid[i]=move_row_right(grid[i])

    return next_grid

def transpose(grid):
    grid_length=len(grid)
    transposee_grid=[]
    for i in range(grid_length):
        new_line=[]
        for j in range(grid_length):
            new_line.append(grid[i][j])
        transposee_grid.append(new_line)
    for i in range(grid_length):
        for j in range(i):
            transposee_grid[j][i], transposee_grid[i][j]=grid[i][j], grid[j][i]
    return transposee_grid


def move_grid_up(grid):
    next_grid=transpose(move_grid_left(transpose(grid)))
    return next_grid


def move_grid_down(grid):
    next_grid=transpose(move_grid_right(transpose(grid)))
    return next_grid


def move_grid(grid, direction):
    grid_length=len(grid)
    next_grid=[]
    for i in range(grid_length):
        new_line=[]
        for j in range(grid_length):
            new_line.append(grid[i][j])
        next_grid.append(new_line)
    if direction == 'g':
        grid=move_grid_left(grid)
        return grid
    elif direction == 'd':
        grid=move_grid_right(grid)
        return grid
    elif direction == 'h':
        grid=move_grid_up(grid)
        return grid
    elif direction == 'b':
        grid=move_grid_down(grid)
        return grid
    elif direction == 'f':
        return 'Fin'
    else:
        return 'Erreur'


def move_possible(grid):
    grid_length=len(grid)
    copy_grid=[]
    for i in range(grid_length):
        new_line=[]
        for j in range(grid_length):
            new_line.append(grid[i][j])
        copy_grid.append(new_line)

    left, right, up, down = True, True, True, True

    if move_grid_left(copy_grid) == grid:
        left = False
    if move_grid_right(copy_grid) == grid:
        right = False
    if move_grid_up(copy_grid) == grid:
        up = False
    if move_grid_down(copy_grid) == grid:
        down = False

    return [left, right, up, down]


def grid_full(grid):
    return (0 not in get_all_tiles(grid))


def game_over(grid):
    move = move_possible(grid)
    full = grid_full(grid)

    if move == [False, False, False, False] and full:
        return True
    else:
        return False


def get_max_tile_value(grid):
    grid_length = len(grid)
    max = 0
    for i in range(grid_length):
        for j in range(grid_length):
            tile_value = get_value_tile(grid, i, j)
            if tile_value > max:
                max = tile_value
    return max


def play_2048():
    size = read_size_grid()
    theme = read_theme_grid()
    grid = init_game(size)
    print(grid_to_string_with_size_and_theme(grid, theme))

    play = True
    while play:
        dir = input('Déplacement ? (f pour terminer le jeu)\n')
        move = move_grid(grid, dir)

        if move == 'Erreur':
            print('Mauvaise commande, recommencer')
        elif move == 'Fin':
            play = False
        else:
            if get_empty_tiles_pos(grid) != []:
                add_new_tile(grid)
            print(grid_to_string_with_size_and_theme(grid, theme))

        if game_over(grid):
            play = False

    if get_max_tile_value(grid) >= 2048:
        print('Partie gagnée !')
    else:
        print('Partie perdue ...')


def random_play():
    size = 4
    grid = init_game(size)
    print(grid_to_string_with_size(grid))
    directions = ['g', 'd', 'b', 'h']

    while not game_over(grid):
        possible = move_possible(grid)
        move = rd.randint(0, 3)
        while possible[move]==False:
            move=rd.randint(0,3)

        d = directions[move]
        move_grid(grid, d)

        add_new_tile(grid)
        print(grid_to_string_with_size(grid))

    if get_max_tile_value(grid) >= 2048:
        print('Partie gagnée !')
    else:
        print('Partie perdue ...')




