#affichage
from tkinter import *
import os
os.chdir('C:\\Users\\maxime\\PycharmProjects\\game2048')
from grid_2048 import *
from textual_2048 import *

TILES_BG_COLOR = {0: "#9e948a", 2: "#eee4da", 4: "#ede0c8", 8: "#f1b078", \
                  16: "#eb8c52", 32: "#f67c5f", 64: "#f65e3b", \
                  128: "#edcf72", 256: "#edcc61", 512: "#edc850", \
                  1024: "#edc53f", 2048: "#edc22e", 4096: "#5eda92", \
                  8192: "#24ba63"}

TILES_FG_COLOR = {0: "#9e948a", 2: "#776e65", 4: "#776e65", 8: "#f9f6f2", \
                  16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2", \
                  256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2", \
                  2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}

TILES_FONT = {"Verdana", 40, "bold"}
END_FONT = {"Verdana", 100, "bold"}


def graphical_grid_init():
    global graphical_grid, GRID_SIZE
    background = Frame(game)
    background.grid()

    for i in range(GRID_SIZE):
        graphical_line = []
        for j in range(GRID_SIZE):
            tile = Frame(background, bg=TILES_BG_COLOR[0], width=TILES_SIZE, height=TILES_SIZE)
            tile.grid(row=i, column=j, padx=1, pady=1)
            label = Label(master=tile, text="", bg=TILES_BG_COLOR[0], \
                        justify=CENTER, font=TILES_FONT, \
                        width=8, height=4)
            label.grid()
            graphical_line.append(label)
        graphical_grid.append(graphical_line)


def display_and_update_graphical_grid(grid):

    global graphical_grid, theme, GRID_SIZE
    for i in range(GRID_SIZE):
        for j in range(GRID_SIZE):
            graphical_grid[i][j].configure(text=THEMES[theme][get_value_tile(grid, i, j)], \
                                        bg=TILES_BG_COLOR[get_value_tile(grid,i,j)], \
                                        fg=TILES_FG_COLOR[get_value_tile(grid,i,j)])
    game.update_idletasks()


def key_pressed(event):
    global grid
    possible_move = move_possible(grid)
    key=event.keysym
    if key=='Left' and possible_move[0]==True:
        grid = move_grid(grid, 'g')
        grid=add_new_tile(grid)
        display_and_update_graphical_grid(grid)
    elif key=='Right' and possible_move[1]==True:
        grid = move_grid(grid, 'd')
        grid=add_new_tile(grid)
        display_and_update_graphical_grid(grid)
    elif key=='Up' and possible_move[2]==True:
        grid = move_grid(grid, 'h')
        grid=add_new_tile(grid)
        display_and_update_graphical_grid(grid)
    elif key=='Down' and possible_move[3]==True:
        grid = move_grid(grid, 'b')
        grid=add_new_tile(grid)
        display_and_update_graphical_grid(grid)
    if game_over(grid):
        if get_max_tile_value(grid)>=2048:
            label = Label(master=game, text="GAGNÉ", bg=TILES_BG_COLOR[0], fg = 'black', \
                        justify=CENTER, font=END_FONT, \
                        width=24, height=12)
            label.grid(column = 0, row = 0)
        else:
            label = Label(master=game, text="PERDU", bg='white', fg = 'black', \
                        justify=CENTER, font=END_FONT, \
                        width=24, height=12)
            label.grid(column = 0, row = 0)

def jouer_2048():
    global GRID_SIZE, grid, theme
    GRID_SIZE=int(spin.get())
    grid=init_game(GRID_SIZE)
    theme=str(list_theme.curselection()[0])
    graphical_grid_init()
    display_and_update_graphical_grid(grid)


global GRID_SIZE
GRID_SIZE=4
global graphical_grid
graphical_grid=[]
global TILES_SIZE
TILES_SIZE = 150
global grid
grid=init_game(GRID_SIZE)
global root
root = Tk()
root.title('2048')
root.resizable(False, False)
global game
game = Toplevel(root)
game.title('2048')
game.bind('<Key>', key_pressed)
game.resizable(False, False)
game.grid()
global theme
theme = '0'
chose_size = Label(root, text="Choose grid size")
spin = Spinbox(root, from_=4, to=8)
chose_theme = Label(root, text="Choose a theme")
list_theme = Listbox(root, selectmode="single")
list_theme.config(height=4)
button = Button(root, text="Play", command=jouer_2048)
button_quit = Button(root, text="Quit", command=quit)
for key in THEMES.keys():
    list_theme.insert(key, THEMES[key]["name"])
chose_size.pack()
spin.pack()
chose_theme.pack()
list_theme.pack()
button.pack(side=RIGHT, padx=5, pady=5)
button_quit.pack(side=LEFT, padx=5, pady=5)


root.mainloop()






